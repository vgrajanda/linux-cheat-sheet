#!/bin/bash

function linux_sheet () {
	clear

	# Title
	echo -e "$(toilet -f future --gay LINUX)"

	# Links
	echo -e "\n$(toilet -f wideterm LINKS)\n"
	echo -e "Need a hand with linux daily use? Check this: https://github.com/jlevy/the-art-of-command-line"

	# Commands
	echo -e "\n\n$(toilet -f wideterm COMMANDS)"
	echo -e "
> CTRL+w - Delete current word
> CTRL+u - Delete all the content
> CTRL+a - Go to start of the line
> CTRL+e - Go to the end of the line

> CTRL+r - Recursive search for history commands (press Ctrl+r againt to iterate)
"
}

function bash_sheet () {
	clear

	# Title
	echo -e "$(toilet -f future --gay "BASH SCRIPTING")"

	# Links
	echo -e "\n$(toilet -f wideterm LINKS)\n"
	echo -e "Need a hand with Bash Scripting? Check this: https://github.com/bobbyiliev/introduction-to-bash-scripting"
}

