#!/bin/bash

source ./src/functions/sheets.sh

function main_menu() {
	clear

	echo -ne "
.::CHOOSE YOUR DESTINIY::.

1) LINUX
2) BASH SCRIPTING 

DESTINY: "
	
	read -r ans

	case $ans in
	1)
		linux_sheet ;;
	2)
		bash_sheet ;;
	0)
		echo "Cya, nerd"
		exit 0 ;;
	*)
		echo "Wrong option!"
		exit 1 ;;
	esac
}

main_menu
